package me.vinceh121.imgenthingy.gens;

import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;

import io.vertx.ext.web.RoutingContext;
import me.vinceh121.imgenthingy.AbstractGenerator;
import me.vinceh121.imgenthingy.ImgenThingy;

public class Clyde extends AbstractGenerator {

	public Clyde(ImgenThingy thingy) {
		super(thingy, "clyde");
	}

	@Override
	public void prepareTemplate(RoutingContext ctx, ChromeDriver driver) {
		setText(driver, "text", ctx.request().getParam("text"));
		setText(driver, "username", ctx.request().getParam("username"));
		setText(driver, "time", ctx.request().getParam("time"));
		setText(driver, "bot", ctx.request().getParam("bot"));
		setText(driver, "onlyyou", ctx.request().getParam("onlyyou"));
		setText(driver, "dismiss", ctx.request().getParam("dismiss"));
	}

	@Override
	public List<String> getParameters() {
		return List.of("text", "username", "time", "bot", "onlyyou", "dismiss");
	}
}
