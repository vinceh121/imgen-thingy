package me.vinceh121.imgenthingy.gens;

import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;

import io.vertx.ext.web.RoutingContext;
import me.vinceh121.imgenthingy.AbstractGenerator;
import me.vinceh121.imgenthingy.ImgenThingy;

public class UnownGB extends AbstractGenerator {

	public UnownGB(ImgenThingy thingy) {
		super(thingy, "unowngb");
	}

	@Override
	public void prepareTemplate(RoutingContext ctx, ChromeDriver driver) {
		setClassToTag(driver, "body", "unownGB");
		setText(driver, "text", ctx.request().getParam("text"));
	}

	@Override
	public List<String> getParameters() {
		return List.of("text");
	}
	
	@Override
	public String getTemplateName() {
		return "unown.html";
	}
}
