package me.vinceh121.imgenthingy.gens;

import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;

import io.vertx.ext.web.RoutingContext;
import me.vinceh121.imgenthingy.AbstractGenerator;
import me.vinceh121.imgenthingy.ImgenThingy;

public class PornHub extends AbstractGenerator {

	public PornHub(ImgenThingy thingy) {
		super(thingy, "pornhub");
	}

	@Override
	public void prepareTemplate(RoutingContext ctx, ChromeDriver driver) {
		setText(driver, "porn", ctx.request().getParam("porn", "Porn"));
		setText(driver, "hub", ctx.request().getParam("hub", "hub"));
	}

	@Override
	public List<String> getParameters() {
		return List.of("porn", "hub");
	}
}
