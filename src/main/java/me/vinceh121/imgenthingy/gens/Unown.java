package me.vinceh121.imgenthingy.gens;

import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;

import io.vertx.ext.web.RoutingContext;
import me.vinceh121.imgenthingy.AbstractGenerator;
import me.vinceh121.imgenthingy.ImgenThingy;

public class Unown extends AbstractGenerator {

	public Unown(ImgenThingy thingy) {
		super(thingy, "unown");
	}

	@Override
	public void prepareTemplate(RoutingContext ctx, ChromeDriver driver) {
		setText(driver, "text", ctx.request().getParam("text"));
	}

	@Override
	public List<String> getParameters() {
		return List.of("text");
	}
}
