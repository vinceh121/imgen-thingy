package me.vinceh121.imgenthingy;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractGenerator {
	protected final Logger log = LogManager.getLogger(getClass());
	private final ImgenThingy thingy;
	private final String name;

	public AbstractGenerator(ImgenThingy thingy, String name) {
		this.thingy = thingy;
		this.name = name;
		thingy.getRouter().get("/" + name).handler(this::handle);
	}

	protected void handle(RoutingContext ctx) {
		this.thingy.getVertx().sharedData().getLocalLock("webdriver").onSuccess(lock -> {
			try {
				ChromeDriver driver = this.getThingy().getDriver();
				driver.get("file://"
						+ this.thingy.getTemplateRoot().resolve(this.getTemplateName()).toAbsolutePath().toString());

				this.prepareTemplate(ctx, driver);

				byte[] png = driver.findElement(By.tagName("body")).getScreenshotAs(OutputType.BYTES);
				ctx.response().putHeader("Content-Type", "image/png").end(Buffer.buffer(png));
			} catch (Exception e) {
				this.error(ctx, e);
			} finally {
				lock.release();
			}
		}).onFailure(t -> this.error(ctx, t));
	}

	protected void error(RoutingContext ctx, Throwable t) {
		this.log.error("Unexpected error", t);
		ctx.response()
				.setStatusCode(500)
				.putHeader("Content-Type", "application/json")
				.end(new JsonObject().put("error", t.toString()).toBuffer());
	}

	public abstract void prepareTemplate(RoutingContext ctx, ChromeDriver driver);
	
	public abstract List<String> getParameters();

	public static void setClassToTag(ChromeDriver driver, String tag, String cls) {
		driver.executeScript("document.getElementsByTagName(arguments[0])[0].className = arguments[1]", tag, cls);
	}
	
	public static void setText(ChromeDriver driver, String id, String text) {
		if (text == null) {
			return;
		}
		driver.executeScript("document.getElementById(arguments[0]).textContent = arguments[1]", id, text);
	}

	public static void setSrc(ChromeDriver driver, String id, String src) {
		if (src == null) {
			return;
		}
		driver.executeScript("document.getElementById(arguments[0]).src = arguments[1]", id, src);
	}

	public ImgenThingy getThingy() {
		return thingy;
	}

	public String getName() {
		return name;
	}

	public String getTemplateName() {
		return this.name + ".html";
	}
}
