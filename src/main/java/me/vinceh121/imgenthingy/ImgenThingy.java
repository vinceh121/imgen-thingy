package me.vinceh121.imgenthingy;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import me.vinceh121.imgenthingy.gens.Achievement;
import me.vinceh121.imgenthingy.gens.Clyde;
import me.vinceh121.imgenthingy.gens.PornHub;
import me.vinceh121.imgenthingy.gens.Unown;
import me.vinceh121.imgenthingy.gens.UnownGB;

public class ImgenThingy {
	private final Vertx vertx;
	private final HttpServer server;
	private final Router router;
	private final ChromeDriver driver;
	private final List<AbstractGenerator> gens = new ArrayList<>();

	public static void main(String[] args) {
		new ImgenThingy().run();
	}

	public ImgenThingy() {
		this.vertx = Vertx.vertx();
		this.server = vertx.createHttpServer();
		this.router = Router.router(vertx);
		this.server.requestHandler(this.router);

		final ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--window-size=2300,1080");
		opt.addArguments("--headless=new");
		opt.addArguments("--remote-allow-origins=*");
		this.driver = new ChromeDriver(opt);

		this.registerGens();

		this.router.get("/").handler(this::handleRoot);
	}

	private void handleRoot(RoutingContext ctx) {
		JsonObject obj = new JsonObject();

		for (AbstractGenerator gen : this.gens) {
			JsonObject objGen = new JsonObject();
			objGen.put("parameters", gen.getParameters());
			obj.put("/" + gen.getName(), objGen);
		}

		ctx.response().putHeader("Content-Type", "application/json").end(obj.toBuffer());
	}

	private void registerGens() {
		gens.add(new PornHub(this));
		gens.add(new Achievement(this));
		gens.add(new Clyde(this));
		gens.add(new Unown(this));
		gens.add(new UnownGB(this));
	}

	public void run() {
		this.server.listen(9100, "127.0.0.1");
	}

	public Path getTemplateRoot() {
		return Path.of(".", "htmlTemplates");
	}

	public Vertx getVertx() {
		return vertx;
	}

	public Router getRouter() {
		return router;
	}

	public ChromeDriver getDriver() {
		return driver;
	}
}
